require('coffee-script');
var docpadInstanceConfiguration = require('./docpad');
require('docpad').createInstance(docpadInstanceConfiguration, function(err,docpadInstance){
    if (err)  return console.log(err.stack);
    
    docpadInstance.action('generate server', function(err,result){
        if (err)  return console.log(err.stack);
        console.log('OK');
    });
});